

function move(dir) {
    console.log(dir);
    if(dir.includes('E')) {
        myPosition.x++;
    }
    if(dir.includes('W')) {
        myPosition.x--;
    }
    if(dir.includes('N')) {
        myPosition.y = myPosition.y + 1;
    }
    if(dir.includes('S')) {
        myPosition.y = myPosition.y - 1;
    }
}

///////////////////----------------///////////////
let myPosition = {x: 5, y: 0};
const treasurePosition = {x: 0, y: 6};

for(let i = 0; i < 10; i++) {
    console.log('CURRENT POSITION', myPosition);
    let direction = '';
    if(myPosition.y < treasurePosition.y) {
        direction += 'N'
    } else if(myPosition.y > treasurePosition.y) {
        direction += 'S'
    }

    if(myPosition.x < treasurePosition.x) {
        direction += 'E'
    } else if(myPosition.x > treasurePosition.x) {
        direction += 'W'
    }
    move(direction);


    if(myPosition.x === treasurePosition.x && myPosition.y === treasurePosition.y) {
        console.info('YOU DO DID IT!');
        break;
    }
    if(i === 9) {
        console.error('NO MORE POWER. GAME OVER!')
    }

}
